module.exports.typeDef = `
  type Product {
    name: String
    price: Float,
    id: String
  }
  type Query {
    products: [Product]
  }
`;
