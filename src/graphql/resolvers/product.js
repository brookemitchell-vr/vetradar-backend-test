const { models } = require('../../models/');

module.exports.resolvers = {
  Query: {
    products: async () => {
      let prods = await models.productModel.getProducts();
      return prods;
    }
  }
};
