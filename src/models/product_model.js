class ProductModel {
  constructor (collections) {
    this.products = collections.products;
  }
  async getProducts (ids) {
    let query = {};
    if (ids && ids.length) {
      query._id = { $in: ids };
    }
    let products = await this.products.find(query);
    return products;
  }
  async getProductByName (name) {
    let product = await this.products.findOne({ name: name });
    return product;
  }
}

module.exports.ProductModel = ProductModel;
