const { graphql } = require('graphql');
const {
  makeExecutableSchema,
  addMockFunctionsToSchema,
  mockServer
} = require('graphql-tools');
const chai = require('chai');
const expect = chai.expect;

const { typeDef: Product } = require('../src/graphql/types/product');
const { typeDef: Cart } = require('../src/graphql/types/cart');

describe('Product Schema', () => {
  const cases = [
    {
      id: 'Test case 1',
      query: `
        query {
          products {
             name,
             price
          }
        }
      `,
      variables: {},
      context: {},
      expected: {
        data: {
          products: [
            {
              name: 'Sledgehammer',
              price: 125.76
            },
            {
              name: 'Sledgehammer',
              price: 125.76
            }
          ]
        }
      }
    }
  ];

  const mockSchema = makeExecutableSchema({ typeDefs: Product });

  addMockFunctionsToSchema({
    schema: mockSchema,
    mocks: {
      Boolean: () => false,
      ID: () => '1',
      Int: () => 1,
      Float: () => 125.76,
      String: () => 'Sledgehammer'
    }
  });

  it('has valid type definitions', async () => {
    expect(async () => {
      const MockServer = mockServer(Product);

      await MockServer.query(`{ __schema { types { name } } }`);
    }).not.throw();
  });

  cases.forEach(obj => {
    const { id, query, variables, context: ctx, expected } = obj;

    it(`query: ${id}`, async () => {
      let result = await graphql(mockSchema, query, null, { ctx }, variables);
      return expect(JSON.stringify(result)).equal(JSON.stringify(expected));
    });
  });
});
describe('Cart Schema', () => {
  const cases = [
    {
      id: 'Test case 1',
      query: `
        query {
          cart (id: 1) {
            products {
              name,
              quantity,
              price,
              total
            },
            total
          }
        }
      `,
      variables: {},
      context: {},
      expected: {
        data: {
          cart: {
            products: [
              {
                name: 'Sledgehammer',
                quantity: 1,
                price: 125.76,
                total: 125.76
              },
              {
                name: 'Sledgehammer',
                quantity: 1,
                price: 125.76,
                total: 125.76
              }
            ],
            total: 125.76
          }
        }
      }
    }
  ];

  const mockSchema = makeExecutableSchema({ typeDefs: Cart });

  addMockFunctionsToSchema({
    schema: mockSchema,
    mocks: {
      Boolean: () => false,
      ID: () => '1',
      Int: () => 1,
      Float: () => 125.76,
      String: () => 'Sledgehammer'
    }
  });

  it('has valid type definitions', async () => {
    expect(async () => {
      const MockServer = mockServer(Product);

      await MockServer.query(`{ __schema { types { name } } }`);
    }).not.throw();
  });

  cases.forEach(obj => {
    const { id, query, variables, context: ctx, expected } = obj;

    it(`query: ${id}`, async () => {
      let result = await graphql(mockSchema, query, null, { ctx }, variables);
      return expect(JSON.stringify(result)).equal(JSON.stringify(expected));
    });
  });
});
