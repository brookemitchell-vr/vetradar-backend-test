module.exports = {
  "plugins": [
    "security"
  ],
  "extends": [
    "semistandard",
    "plugin:promise/recommended",
    "plugin:security/recommended"
  ],
  "parserOptions": { "ecmaVersion": 6 },
  "env": {
    "es6": true,
    "node": true
  }
};